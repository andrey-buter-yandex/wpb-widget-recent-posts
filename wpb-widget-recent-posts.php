<?php
/*
Plugin Name: WPB Recent Posts Widget
Description: Change default Recent Posts widget to modified widget.
Author: WPB
Version: 1.0
Author URI: http://wordpressforbroadcasters.com/
License: GPLv2 or later
Text Domain: rssi
Domain Path: /i18n
*/

/**
 * Current plugin version
 * Each time on plugin initialization 
 * we are checking this version with one stored in plugin options
 * and if they don't match plugin update hook will be fired
 *
 * @since 2.1
 */
if ( !defined( 'WPBW_RP_VERSION' ) )
	define( 'WPBW_RP_VERSION', '1.0' );

/**
 * The name of the SocialFlow Core file
 *
 * @since 2.0
 */
if ( !defined( 'WPBW_RP_FILE' ) )
	define( 'WPBW_RP_FILE', __FILE__ );

/**
 * Absolute location of SocialFlow Plugin
 *
 * @since 2.0
 */
if ( !defined( 'WPBW_RP_ABSPATH' ) )
	define( 'WPBW_RP_ABSPATH', dirname( WPBW_RP_FILE ) );

/**
 * The name of the SocialFlow directory
 *
 * @since 2.0
 */
if ( !defined( 'WPBW_RP_DIRNAME' ) )
	define( 'WPBW_RP_DIRNAME', basename( WPBW_RP_ABSPATH ) );

/*
 * The name of the SocialFlow directory
 *
 * @since 2.0
 */
if ( !defined( 'WPBW_RP_URI' ) )
	define( 'WPBW_RP_URI', plugins_url( '', WPBW_RP_FILE ) );


/**
 * WPB_Widget_Pecent_Posts_Plugin
 */
class WPB_Widget_Pecent_Posts_Plugin
{	
	public static function load()
	{
		add_action( 'plugins_loaded', array( __CLASS__, 'load_templates' ) );
		add_action( 'widgets_init', array( __CLASS__, 'widget_init' ) );
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'admin_autosubmit_widget' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'scripts_styles' ) );
	}

	function load_templates()
	{
		require_once( WPBW_RP_ABSPATH . '/inc/widget.php' );
		// require_once( WPBW_RP_ABSPATH . '/inc/script-loader.class.php' );
	}

	function widget_init()
	{
		// Replace Recent comemnts widget
		unregister_widget( 'WP_Widget_Recent_Posts' );
		register_widget( 'WPB_Widget_Recent_Posts' );
	}

	/**
	 * Auto update widget script for recent posts widget
	 * @return void
	 */
	function admin_autosubmit_widget() 
	{
		$path = WPBW_RP_URI .'/assets';

		wp_enqueue_script( 'wpbw_rp_autosubmit_widget', $path .'/wpb-recent-posts-widget-admin.js', array( 'jquery' ), '1.0', true );
	}

	function scripts_styles()
	{
		$path = WPBW_RP_URI .'/assets';

		 /* Load slider lib */
		wp_enqueue_script( 'wpbw-rp-jquery.touchSwipe', $path .'/caroufredsel/jquery.touchSwipe.min.js', array( 'jquery' ), '1.3.3', true );

		wp_enqueue_script( 'wpbw-rp-carouFredSel', $path .'/caroufredsel/jquery.caroufredsel-packed.js', array( 'jquery', 'jquery.touchSwipe' ), '6.2.1', true );

		wp_enqueue_script( 'wpbw-rp-common', $path .'/wpb-recent-posts-widget-common.js', array( 'jquery' ), '6.2.1', true );
	}
}

WPB_Widget_Pecent_Posts_Plugin::load();