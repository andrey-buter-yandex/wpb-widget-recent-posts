<?php 

/**
 * Recent_Posts widget class
 *
 * @since 1.0
 */


class WPB_Widget_Recent_Posts extends WP_Widget_Recent_Posts 
{
	/**
	 * Hold available template options for widget
	 * @var array
	 */
	public $template_options;

	function __construct() 
	{
		$widget_ops = array( 
			'classname'   => 'widget_recent_posts', 
			'description' => __( "The most recent posts on your site" ) 
		);

		WP_Widget::__construct( 'wpbw-recent-posts', __( 'WPB Recent Posts' ), $widget_ops );

		$this->alt_option_name = 'widget_recent_posts';

		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );

		$this->template_options = array(
			'summary'      => __( 'Include summary', 'itmwp2' ),
			'thumbnail'    => __( 'Include thumbnail', 'itmwp2' ),
			'archive_link' => __( 'Include archive link', 'itmwp2' ),
			'slider'       => __( 'Output as slider', 'itmwp2' ),
		);
	}

	function widget($args, $instance) {
		$cache = wp_cache_get( 'widget_recent_posts', 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract( $args );

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Posts' ) : $instance['title'], $instance, $this->id_base );

		$r = new WP_Query( $this->query_args( $instance ) );
		
		$template_settings = isset( $instance['template'] ) ? (array) $instance['template'] : array();

		$container_class = '';

		$template = $first_template = '';

		if ( in_array( 'archive_link', $template_settings ) !== false && $r->post_count > 1 ) {

			// If slider is checked too don't insert link icon
			if ( in_array( 'slider', $template_settings ) !== false ) {
				$title = sprintf( '<a href="%s">%s</a>', $this->get_archive_url( $instance ), $title );
			} else {
				$title = sprintf( '<a href="%s">%s <i class="icon-right-open color-brand title-link"></i></a>', $this->get_archive_url( $instance ), $title );
			}
		}

		// Add navigation right after widget title
		if ( in_array( 'slider', $template_settings ) !== false && $r->have_posts() ) {
			$title .= '<span class="prev-next-nav">'.
				'<button class="secondary btn-small icon-left-open color-brand" id="js-'. $widget_id .'-prev"></button>'.
				'<button class="secondary btn-small icon-right-open color-brand" id="js-'. $widget_id .'-next"></button>'.
			'</span>';

			$container_class .= ' js-posts-slider';

			// Add specific class to widget container
			$before_widget = str_replace( 'widget_recent_posts', 'widget_recent_posts recent_posts_slider', $before_widget);

		}

		// Render first special template for thumb template only in content area
		if ( in_array( 'thumbnail', $template_settings ) !== false ) {
			$template = 'thumbnail';

			// Use specific template for first thumbnailed article when inside content area
			// if ( $id == 'content-1' ) {
			// 	$first_template .= $template . '-content-first';
			// }
		}

		// Special template if summary is enabled
		if ( in_array( 'summary', $template_settings ) !== false ) {
			$template .= empty( $template ) ? 'summary' : '-summary';
		}
?>
		<?php echo $before_widget; ?>
		<?php if ( $title ) echo $before_title . $title . $after_title; ?>
		<?php if ( $r->have_posts() ) : ?>
			<div class="recent-posts-list posts-list">
				<div class="posts-container <?php echo $container_class; ?>" id="js-<?php echo $widget_id ?>">
				<?php while ( $r->have_posts() ) : $r->the_post();
					get_theme_part( 'widget/recent', ( is_first_post( $r ) && !empty( $first_template ) ) ? $first_template : $template );
				endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();
		unset( $GLOBALS['show_recent_post_episode'] );

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('widget_recent_posts', $cache, 'widget');
	}

	/**
	 * Update also post_type variable
	 */
	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;

		$instance['title']     = strip_tags( $new_instance['title'] );
		$instance['post_type'] = strip_tags( $new_instance['post_type'] );
		$instance['taxonomy']  = isset( $new_instance['taxonomy']) ? strip_tags( $new_instance['taxonomy'] ) : '';
		$instance['term']      = isset( $new_instance['term']) ? strip_tags( $new_instance['term'] ) : '';
		$instance['number']    = isset( $new_instance['number'] ) ? (int) $new_instance['number'] : 5;

		$instance['template']  = array();
		// Store all template options in array
		foreach ( $this->template_options as $key => $value ) {
			if ( isset( $new_instance['template-' . $key ] ) ) {
				$instance['template'][] = $key;
			}
		}

		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		return $instance;
	}

	/**
	 * Render post_type select in addition to other form elements
	 */
	function form( $instance ) 
	{
		$title             = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$current_post_type = $this->_get_current_post_type( $instance );
		$current_taxonomy  = isset( $instance['taxonomy'] ) ? esc_attr( $instance['taxonomy'] ) : '';
		$current_term      = isset( $instance['term'] ) ? esc_attr( $instance['term'] ) : '';
		$number            = isset( $instance['number'] ) ? absint( $instance['number'] ) : 3;
		$template          = isset( $instance['template'] ) ? $instance['template'] : array();
?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('post_type'); ?>"><?php _e('Post Type:') ?></label>
			<select class="widefat js-itmwp-widget-submit-onchange" id="<?php echo $this->get_field_id('post_type'); ?>" name="<?php echo $this->get_field_name('post_type'); ?>">
			<?php foreach ( get_post_types( null, 'objects' ) as $post_type ) : ?>
				<option value="<?php echo esc_attr($post_type->name) ?>" <?php selected($post_type->name, $current_post_type) ?>><?php echo $post_type->labels->name; ?></option>
			<?php endforeach; ?>
			</select>
		</p>

		<?php if ( $taxonomies = get_object_taxonomies( $current_post_type ) ) : ?>
			<p>
				<label for="<?php echo $this->get_field_id('taxonmy'); ?>"><?php _e('Taxonomy:') ?></label>
				<select class="widefat js-itmwp-widget-submit-onchange" id="<?php echo $this->get_field_id('taxonomy'); ?>" name="<?php echo $this->get_field_name('taxonomy'); ?>">
					<option><?php _e( 'Non categorized widget', 'itmwp2' ); ?></option>
				<?php foreach ( $taxonomies as $taxonomy ) : ?>
					<option value="<?php echo esc_attr($taxonomy); ?>" <?php selected($taxonomy, $current_taxonomy) ?>><?php echo $taxonomy; ?></option>
				<?php endforeach; ?>
				</select>
			</p>

			<?php if ( in_array( $current_taxonomy, $taxonomies ) !== false ) : ?>
			<p>
				<label for="<?php echo $this->get_field_id('taxonmy'); ?>"><?php printf( __( 'Select %s:' ), $current_taxonomy ) ?></label>
				<?php wp_dropdown_categories(array( 
					'name' => $this->get_field_name('term'), 
					'id' => $this->get_field_id('term'), 
					'selected' => $current_term, 
					'taxonomy' => $current_taxonomy, 
					'class' => 'widefat',
					'show_option_all' => __( 'Non categorized widget', 'itmwp2' )
				)); ?>
			</p>
			<?php endif; ?>

		<?php endif; ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
		</p>

		<?php foreach ( $this->template_options as $key => $label ): ?>
			<p>
				<label for="<?php echo $this->get_field_id( 'template-' .$key ); ?>"><?php echo $label; ?></label>
				<input type="checkbox" name="<?php echo $this->get_field_name("template-$key"); ?>" id="<?php echo $this->get_field_id( 'template-' . $key ); ?>" value="1" <?php checked( in_array( $key, $template ), true ) ?> />
			</p>
		<?php endforeach ?>

		<?php
	}

	/**
	 * Get query args for recent posts widgets
	 * @param  array $instance widget instance
	 * @return array
	 */
	function query_args( $instance ) 
	{
		$args = array(
			'no_found_rows'       => true, 
			'post_status'         => 'publish', 
			'ignore_sticky_posts' => true,
			'post_type'           => $this->_get_current_post_type( $instance ),
			'posts_per_page'      => absint( $instance['number'] ) ? absint( $instance['number'] ) : 10
		);

		// Term must be specified for particular post object
		if ( isset( $instance['taxonomy'] ) && 
			 isset( $instance['term'] ) && 
			 false !== in_array( $instance['taxonomy'], get_object_taxonomies( $args['post_type'] ) ) &&
			 term_exists( absint( $instance['term'] ), $instance['taxonomy'] )
		) {
			$args['tax_query'] = array(array(
				'taxonomy' => $instance['taxonomy'],
				'field' => 'id',
				'terms' => absint( $instance['term'] )
			));
		}

		if ( isset( $instance['show_audio'] ) ) {
			$args['show_audio'] = true;
		}

		return apply_filters( 'widget_posts_args', $args );
	}

	/**
	 * Get archive url
	 * @param  array $instance
	 * @return string
	 */
	function get_archive_url( $instance ) 
	{
		$url = '';

		// Term taxonomy url
		if ( isset( $instance['taxonomy'] ) && 
			 isset( $instance['term'] ) && 
			 false !== in_array( $instance['taxonomy'], get_object_taxonomies( $this->_get_current_post_type( $instance ) ) ) &&
			 term_exists( absint( $instance['term'] ), $instance['taxonomy'] )
			) {
			$url = get_term_link( absint( $instance['term'] ), $instance['taxonomy'] );

			if ( is_wp_error( $url ) ) {
				return '';
			}
		}

		return $url;
	}

	/**
	 * Get current post type from passed instance
	 * @param  array $instance
	 * @return string post type
	 */
	function _get_current_post_type( $instance ) 
	{
		if ( !empty( $instance['post_type'] ) && post_type_exists( $instance['post_type'] ) )
			return $instance['post_type'];

		return 'post';
	}
}