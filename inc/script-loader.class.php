<?php 

/**
 * 
 */

WPB_Filter_Script_Dublicate::load();

class WPB_Filter_Script_Dublicate
{
	public static function load()
	{
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'filter_dublicates' ), 100 );
	}

	function filter_dublicates()
	{
		global $wp_scripts;

		var_dump($wp_scripts);
	}


	/**
	 * find enabled caroufredsel script
	 */
	// function find_enabled_script()
	// {
	// 	global $wp_scripts;

	// 	if ( !$wp_scripts->registered )
	// 		return;

	// 	foreach ( $wp_scripts->registered as $script_data ) {
	// 		if ( false === strpos( $script_data->src, 'carouFredSel' )
	// 			continue;

	// 		return true;
	// 	}

	// 	var_dump($wp_scripts->registered);
	// }

	/**
	 * Check is enable scripts in some active widget
	 */
	private static function is_enable_scripts_in_widget( $widget_data )
	{
		$option_name = $widget_data['callback'][0]->option_name;

		$options = get_option( $option_name );

		if ( !$options )
			return false;

		foreach ( $options as $widget_options ) {
			if ( true === $widget_options['enable_scripts'] )
				return true;
		}

		return false;
	}

	/**
	 * Check is enable scripts
	 */
	private static function is_enable_scripts()
	{
		global $wp_registered_widgets;

		foreach ( $wp_registered_widgets as $widget_id => $widget_data ) {
			$id_base = _get_widget_id_base( $widget_id );

			if ( self::$widget_base_id != $id_base )
				continue;

			return self::is_enable_scripts_in_widget( $widget_data );
		}

		return false;
	}

	/**
	 * Check is active widget in some sidebar
	 */
	private static function is_active_widget()
	{
		return is_active_widget( false, false, self::$widget_base_id, true );
	}
}