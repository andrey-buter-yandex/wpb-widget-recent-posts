"use strict";

jQuery(document).ready(function($) {

	// Initialize slider
	if ( typeof $().carouFredSel == 'function' ) {

		// Featured posts slider
		$('.js-wpbw-recent-posts-slider').each(function(i, e) {
			var itemsContainer = $(this);

			itemsContainer.carouFredSel({
				responsive : true,
				swipe : true,
				items : {
					visible : 1,
				},
				scroll : {
					fx: "directscroll"
				},
				auto : false,
				next : '#' + itemsContainer.attr('id') + '-next',
				prev : '#' + itemsContainer.attr('id') + '-prev'
			});
		});
	}

}) // End of jQuery shell function